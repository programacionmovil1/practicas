package com.example.prac01;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class GradosActivity extends AppCompatActivity {
    private EditText txtTexto;
    private TextView txtResultado;
    private Button btnCalcular, btnLimpiar, btnCerrar;
    private RadioButton rcaf, rfac;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_grados);

        iniciarComponentes();


        Button btnCalcular = findViewById(R.id.btnCalcular);
        Button btnLimpiar = findViewById(R.id.btnLimpiar);
        Button btnCerrar = findViewById(R.id.btnCerrar);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String texto = txtTexto.getText().toString().trim();

                if (texto.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Por favor ingresa un valor antes de calcular", Toast.LENGTH_SHORT).show();
                    return;
                }

                calcularConversion();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtTexto.setText("");
                txtResultado.setText("Resultado");
                rcaf.setChecked(false);
                rfac.setChecked(false);
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }
    private void calcularConversion() {
        String entrada = txtTexto.getText().toString().trim();

        if (!entrada.isEmpty()) {
            double grados = Double.parseDouble(entrada);

            if (rcaf.isChecked()) {
                double fahrenheit = (grados * 9 / 5) + 32;
                txtResultado.setText(String.format("%.2f °F", fahrenheit));
            } else if (rfac.isChecked()) {
                double celsius = (grados - 32) * 5 / 9;
                txtResultado.setText(String.format("%.2f °C", celsius));
            }
        } else {
            txtResultado.setText("Ingrese los grados a convertir");
        }
    }
    public void iniciarComponentes() {
        txtTexto = findViewById(R.id.txtTexto);
        txtResultado = findViewById(R.id.txtResultado);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnCerrar = findViewById(R.id.btnCerrar);
        rcaf = findViewById(R.id.rcaf);
        rfac = findViewById(R.id.rfac);
    }
}