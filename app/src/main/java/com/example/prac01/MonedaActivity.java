package com.example.prac01;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import java.util.HashMap;
import java.util.Map;

public class MonedaActivity extends AppCompatActivity {

    private Spinner spiConversion;
    private EditText txtCantidad;
    private TextView txtResultado;
    private Button botonCalcular, botonLimpiar, botonCerrar;

    private Map<String, Double> tasasDeCambio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moneda);
        getWindow().setStatusBarColor(getResources().getColor(R.color.black));

        // Inicializar componentes de UI
        spiConversion = findViewById(R.id.spinner_conversion);
        txtCantidad = findViewById(R.id.editText_cantidad);
        txtResultado = findViewById(R.id.textView_resultado);
        botonCalcular = findViewById(R.id.boton_calcular);
        botonLimpiar = findViewById(R.id.boton_limpiar);
        botonCerrar = findViewById(R.id.boton_cerrar);

        // Inicializar tasas de cambio
        tasasDeCambio = new HashMap<>();
        tasasDeCambio.put("Pesos a Dólares", 0.06);
        tasasDeCambio.put("Pesos a Euros", 0.0552);
        tasasDeCambio.put("Pesos a Dólar Canadiense", 0.0818);
        tasasDeCambio.put("Pesos a Libra Esterlina", 0.047);

        // Establecer listener de insets de ventana
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.layout_principal), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        botonCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calcularConversion();
            }
        });

        botonLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limpiarCampos();
            }
        });

        botonCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void calcularConversion() {
        String conversionSeleccionada = spiConversion.getSelectedItem().toString();
        String cantidadStr = txtCantidad.getText().toString();

        if (cantidadStr.isEmpty()) {
            Toast.makeText(this, "Por favor, ingrese una cantidad", Toast.LENGTH_SHORT).show();
            return;
        }

        double cantidad = Double.parseDouble(cantidadStr);
        double tasa = tasasDeCambio.get(conversionSeleccionada);
        double resultado = cantidad * tasa;

        txtResultado.setText(String.format("Resultado: %.2f", resultado));
    }

    private void limpiarCampos() {
        txtCantidad.setText("");
        txtResultado.setText("Resultado");
        spiConversion.setSelection(0);
    }
}
