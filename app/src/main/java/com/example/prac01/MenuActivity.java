package com.example.prac01;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MenuActivity extends AppCompatActivity {
    private CardView crvHola ,crvImc, crvGrados, crvMoneda, crvCotizacion, crvSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_menu);
        iniciarComponentes();
        crvHola.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent intent = new Intent (MenuActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
        crvImc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (MenuActivity.this,
                        ImcActivity.class);
                startActivity(intent);
            }
        });
        crvGrados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (MenuActivity.this,
                        GradosActivity.class);
                startActivity(intent);
            }
        });
        crvMoneda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (MenuActivity.this,
                        MonedaActivity.class);
                startActivity(intent);
            }
        });
        crvCotizacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (MenuActivity.this,
                        IngresaCotizacionActivity.class);
                startActivity(intent);
            }
        });
        crvSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (MenuActivity.this,
                        SpinnerActivity.class);
                startActivity(intent);
            }
        });
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }
    public void iniciarComponentes(){
        crvHola = (CardView) findViewById(R.id.crvHola);
        crvImc = (CardView) findViewById(R.id.crvIMC);
        crvGrados = (CardView) findViewById(R.id.crvConversion);
        crvMoneda = (CardView) findViewById(R.id.crvCambio);
        crvCotizacion = (CardView) findViewById(R.id.crvCotizacion);
        crvSpinner = (CardView) findViewById(R.id.crvSpinner);
    }
}